class TasksController < ApplicationController
before_action :authenticate_user!
  def new
    @categories = Category.all
  end

  def create
    @task = Task.new(task_params)
    @category = Category.find(category_params)
    @task.category = @category
    if @task.save
      respond_to do |format|
        format.html { redirect_to root_path
                      flash[:notice] = "Task created" }
        format.js { }
      end
    else
      respond_to do |format|
        format.html { redirect_to root_path
                      flash[:notice] = "Please try again" }
        # format.js { }
      end
    end
  end

  def edit
    @task = Task.find(params[:id])
    @categories = Category.all

  end


  def update
    # https://github.com/rails/jquery-ujs/issues/440
    @task = Task.find(params[:id])
    @task.update(status: params[:status])
    respond_to do |format|
      format.html { redirect_to tasks_path
                    flash[:notice] = "Task edited" }
      format.js { }
    end
    
  end

  def index
    @tasks = Task.all
  end

  def destroy
    @task = Task.find(params[:id])
    puts "============================= #{@task.id}"
    puts "================== #{@task.title}"
    @task.destroy
    respond_to do |format|
      format.html { redirect_to root_path
                    flash[:notice] = "Task deleted" }
      format.js { }
    end
  end


  private

  def task_params
    params.permit(:title, :deadline, :description)
  end

  def category_params
    params.require(:Category)
  end

end
